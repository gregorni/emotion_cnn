# Emotion_CNN

## About

A neural network that is supposed to recognize facial expressions made with Keras and Tensorflow.

I used part of [KDEF and AKDEF](https://kdef.se/) as dataset for training and testing, as well as the publicly available pictures of the [FACESset](https://faces.mpdl.mpg.de/imeji/).

The trained expressions are:
- Afraid
- Angry
- Disgusted
- Happy
- Neutral
- Sad
- Surprised

Small note on the side: This is my first ever larger programming project.

## Sorry...

Unfortunately, I structured the original files really badly, scattered them all over the place and made them reference each other, so I can't really publish them. Until I clean up that mess, you'll have to do with this.

These file I uploaded are 
- a saved neural networked, can be loaded using tf.keras.models.load_model(<"path to modelsave folder">)
- a webcam capturing class
- the actual program, which evaluates webcam input live.

## Usage

Download this repository with 

```bash
git clone https://gitlab.com/gregorni/emotion_cnn.git
```

To execute the main program, run within the emotion_cnn folder:

```bash
python main_program.py
```

## Training information

I used 854 pictures for training and 213 for validation, 1067 in total.

I trained this model for a bit more than 50 epochs.

In the end, the network had about 80% accuracy on test files.

## Support

If you encounter trouble, feel free to open a discussion under the Issues section. However, this code is hardly mine, I hardly understand it, so it might be I can't help you.

## License

This project is licensed under GNU GPLv3, so you may do anything with it *except* distributing a closed source version.
