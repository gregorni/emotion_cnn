import os
import PIL
import PIL.Image
import tensorflow as tf
import numpy as np
import pathlib
from camera_capturing import *

# Define picture dimensions
img_height = 281
img_width = 281

# Define classnames
class_names = ['angry', 'afraid', 'disgusted', 'happy', 'neutral', 'sad', 'surprised']


'''This callback-routine gets called by the Capture-class constantly.
You can specify an image_array as a parameter, which will be processed and can be evaluated by the model using predict.
The return value is a string, which gets displayed at the bottom of the window.'''

def predict_callback(im_array):
    image=im_array.copy()
    image = image.resize((img_height, img_width))
    image = np.expand_dims(image, axis=0)
    single_test = model.predict(image)
    result=class_names[np.argmax(single_test[0])]
    return result


# load a saved model which has been saved earlier with model.save('./latest_modelsave')
model=tf.keras.models.load_model("./latest_modelsave")


# instantiate Class
capture = Capture(0) # the parameter is the number the OS associates with the webcam - just try out until you find the right number
# Run main function with callback as parameter
capture.main(predict_callback)
