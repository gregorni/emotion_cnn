import pygame
import pygame.camera
from PIL import Image
from pygame.locals import *

class Capture(object):
    def __init__(self,device_nr):
        pygame.init()
        pygame.camera.init()
        self.size = (640,480)
        # create a display surface. standard pygame stuff
        self.display = pygame.display.set_mode(self.size, 0)

        # this is the same as what we saw before
        self.clist = pygame.camera.list_cameras()
        if not self.clist:
            raise ValueError("Sorry, no cameras detected.")
        self.cam = pygame.camera.Camera(self.clist[device_nr], self.size)
        self.cam.start()

        # create a surface to capture to.  for performance purposes
        # bit depth is the same as that of the display surface.
        self.snapshot = pygame.surface.Surface(self.size, 0, self.display)

    def get_and_flip(self):
        # if you don't want to tie the framerate to the camera, you can check
        # if the camera has an image ready.  note that while this works
        # on most cameras, some will never return true.
        if self.cam.query_image():
            self.snapshot = self.cam.get_image(self.snapshot)

        # blit it to the display surface.  simple!
        self.display.blit(self.snapshot, (0,0))
        return self.snapshot


    def main(self, image_callback = None):
        going = True
        while going:
            events = pygame.event.get()
            for e in events:
                if e.type == pygame.QUIT or (e.type == pygame.KEYDOWN and e.key == pygame.K_ESCAPE):
                    # close the camera safely
                    self.cam.stop()
                    pygame.display.quit()
                    going = False
            image_surface = self.get_and_flip()
            if image_callback:
                image_array = pygame.surfarray.array3d(image_surface)
                pil_string_image = pygame.image.tostring(image_surface, "RGB", False)
                im = Image.frombytes("RGB", self.size, pil_string_image)
                result = image_callback(im)
                font = pygame.font.SysFont(None, 32)
                white = pygame.Color('white')
                text = font.render(result, True,white)
                textRect = text.get_rect()
                ws_x,ws_y=pygame.display.get_window_size()
                textRect.center = (ws_x//2,ws_y*0.9)
                self.display.blit(text, textRect)
                pygame.display.update()
